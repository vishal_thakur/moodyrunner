﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : MonoBehaviour
{
    //[System.Serializable]
    //public class PlayerInfo
    //{
    //    //Represents this Player's Mood
    //    public Mood CurrentMood;
    //}

    //Represent the Types of Emotional Moods
    public enum Mood
    {
        Chill = 0,
        Happy,
        Angry,
        Upset
    }
    
    //Represents the type of UI panels
    public enum UIPanel
    {
        HUD = 0,
        MainMenu,
        GameOver,
        Pause
    }

    //Represents the Type of Entities
    public enum EntityType
    {
        Evil = 0,
        Angel
    }



    //Represents the max health of the player
    public const int MaxHealth = 3;
}

