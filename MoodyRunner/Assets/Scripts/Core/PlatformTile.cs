﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTile : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;
    public Entity[] obstacles; //Objects that contains different obstacle types which will be randomly activated

    public void ActivateRandomObstacle()
    {
        DeactivateAllObstacles();

        System.Random random = new System.Random();
        int randomNumber = random.Next(0, obstacles.Length);
        if(obstacles.Length > 0)
            obstacles[randomNumber].gameObject.SetActive(true);
    }

    public void DeactivateAllObstacles()
    {
        for (int i = 0; i < obstacles.Length; i++)
        {

            //Reset Rotation
            obstacles[i].transform.eulerAngles = new Vector3(0,-180,0);

            //Disable Obstacle GO
            obstacles[i].gameObject.SetActive(false);

        }
    }

    
}