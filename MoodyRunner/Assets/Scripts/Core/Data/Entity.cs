﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    //Represents the type of entity this is
    public Globals.EntityType type;


    [SerializeField]
    float RotationSpeed = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RotateBackwards()
    {
        StartCoroutine(RotateBackwardsBehaviour());
    }

    IEnumerator RotateBackwardsBehaviour()
    {
        while (!Mathf.Approximately(transform.eulerAngles.y, 0))
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles  , Vector3.zero , Time.deltaTime * RotationSpeed);
            yield return null;
        }

        StopAllCoroutines();
    }
}
