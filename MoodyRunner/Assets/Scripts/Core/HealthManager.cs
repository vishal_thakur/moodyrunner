﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    //Represents the Health units of the player
    [SerializeField]
    List<GameObject> HealthHearts = new List<GameObject>();

    [SerializeField]
    int totalHealth;

    

    private void Start()
    {
        Initialize();
    }

    void Initialize()
    {
        //Get the Max health
        totalHealth = Globals.MaxHealth;

        ShowHeartsAsPerHealth();
    }

    //Used to reduce health by a certain count
    //return true = player dead
    //return false = player alive
    public bool ReduceHealth(int count)
    {
        //Reduce health
        totalHealth--;

        ShowHeartsAsPerHealth();

        if (totalHealth > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    void ShowHeartsAsPerHealth()
    {
        //Show Equivalent Hears wrt totalHealth remaining
        for (int i = 0; i < Globals.MaxHealth; i++)
        {
            if (i < totalHealth)
                HealthHearts[i].SetActive(true);
            else
                HealthHearts[i].SetActive(false);

        }
    }
}
