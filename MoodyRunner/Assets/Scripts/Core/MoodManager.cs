﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoodManager : MonoBehaviour
{

    //Represents the Current Mood
    public Globals.Mood currentMood;

    //Represents Visuals Representing a mood
    [SerializeField]
    List<GameObject> MoodVisuals = new List<GameObject>();

    [SerializeField]
    GameObject MoodBubbleRef;

    //Called to Set a mood
    public void SetMood(Globals.Mood newMood)
    {
        currentMood = newMood;

        for (int i = 0; i < MoodVisuals.Count; i++)
        {
            if (i == (int)newMood)
                MoodVisuals[i].SetActive(true);
            else
                MoodVisuals[i].SetActive(false);
        }
    }

    public void HideMoodBubble()
    {
        MoodBubbleRef.SetActive(false);
    }
    

    //Toggle between Moods
    public void ToggleMood(Globals.Mood mood1, Globals.Mood mood2)
    {
        if (currentMood == mood1)
            SetMood(mood2);
        else
            SetMood(mood1);
    }
}
