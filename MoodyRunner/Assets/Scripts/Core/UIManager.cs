﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    //Represents the Currently Active UI Panel amongst all
    [SerializeField]
    Globals.UIPanel CurrentlyActiveUIPanel;

    //List of all possible UI panels
    [SerializeField]
    List<UIPanel> UIPanels = new List<UIPanel>();


    //Show a Specfic UI Panel
    public void ToggleUIPanel(Globals.UIPanel panelToShow,bool ActiveFlag)
    {
        //Show the Specified UIPanel and Hide others
       foreach(UIPanel panel in UIPanels)
        {
            if (panel.Type == panelToShow)
                panel.gameObject.SetActive(ActiveFlag);
            else
                panel.gameObject.SetActive(false);
        }
    }
}
