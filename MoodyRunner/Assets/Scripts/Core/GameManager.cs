﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Manager Refs
    [SerializeField]
    MoodManager MoodManagerRef;

    [SerializeField]
    Animator PlayerAnimatorRef;

    [SerializeField]
    UIManager UIManagerRef;

    [SerializeField]
    HealthManager HealthManagerRef;

    [SerializeField]
    GroundGenerator GroundGeneratorRef;
    #endregion

    [SerializeField]
    Text ScoreText;


    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    // Start is called before the first frame update
    void Start()
    {
        Initialize();   
    }
    

    void Initialize()
    {
        //Set Initial Mood
        MoodManagerRef.SetMood(Globals.Mood.Chill);

        //Hide HUD UI
        //UIManagerRef.ToggleUIPanel(Globals.UIPanel.HUD, false);

        //Show Splash UI
        UIManagerRef.ToggleUIPanel(Globals.UIPanel.MainMenu, true);
    }

    public void StartGame()
    {
        //Set Mood for Game Start
        MoodManagerRef.SetMood(Globals.Mood.Happy);

        //Change Player animator trigger State
        PlayerAnimatorRef.SetTrigger("startRunning");

        //Show HUD UI
        UIManagerRef.ToggleUIPanel(Globals.UIPanel.HUD, true);

        //Start the Gameplay Behaviour
        StartCoroutine(GameplayBehaviour());
    }
    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void PauseGame()
    {
        //Set Game time Scale to 0
        Time.timeScale = 0;
        
        //Show Pause Menu
        UIManagerRef.ToggleUIPanel(Globals.UIPanel.Pause, true);
    }

    public void ResumeGame()
    {
        //Reset Zero Timescale
        Time.timeScale = 1;

        //Show HUD Menu
        UIManagerRef.ToggleUIPanel(Globals.UIPanel.HUD, true);
    }


    public void SetScore(int score)
    {
        ScoreText.text = ": " + score;
    }

    #region Callbacks
    //Depending on the Current mood the Game manager will decide wether to reduce life or to continue Game
    public void OnPlayerInteractionWithEntity(Globals.EntityType entityType , Entity entity)
    {
        //Signal Entity to rotate backwards
        entity.RotateBackwards();


        //If Player Interacted with a Demon
        if(entityType == Globals.EntityType.Evil)
        {
            //Continue Game
            if(MoodManagerRef.currentMood == Globals.Mood.Angry)
            {
                //Do Correct Mood selected Animation
            }
            else//Reduce Life
            {
                //Life--
                if (!HealthManagerRef.ReduceHealth(1))
                {
                    StartCoroutine(GameOverBehaviour(entity));
                }
                else
                    StumbleBehaviour(entity);

            }
        }
        //If Player Interacted with a Angel
        else if (entityType == Globals.EntityType.Angel)
        {
            //Continue Game
            if (MoodManagerRef.currentMood == Globals.Mood.Happy)
            {
                //Do Correct Mood selected Animation
            }
            else//Reduce Life
            {
                //Life--
                if (!HealthManagerRef.ReduceHealth(1))
                {
                    StartCoroutine(GameOverBehaviour(entity));
                }
                else
                {
                    StumbleBehaviour(entity);
                }
            }

        }
    }

    #endregion


    #region Behaviours

    public void AngryMoodBehaviour()
    {
        MoodManagerRef.SetMood(Globals.Mood.Angry);
    }

    public void HappyMoodBehaviour()
    {
        MoodManagerRef.SetMood(Globals.Mood.Happy);
    }

    //Represents what happens upon Game over
    //Entity represents the one player just interacted with
    IEnumerator GameOverBehaviour(Entity entity)
    {
        //Show Upset Mood
        MoodManagerRef.SetMood(Globals.Mood.Upset);

        yield return new WaitForSeconds(.5f);

        //Hide Mood Bubble
        MoodManagerRef.HideMoodBubble();

        //Tell Ground Generator to stop
        GroundGeneratorRef.gameOver = true;

        //Stop Game Play Behaviour
        StopCoroutine(GameplayBehaviour());


        //Show Player's and entity's Game over Animation
        PlayerAnimatorRef.SetTrigger("gameOver");
        entity.GetComponent<Animator>().SetTrigger("gameOver");

        //Show game over menu
        UIManagerRef.ToggleUIPanel(Globals.UIPanel.GameOver, true);
    }

    //Player Stumbles when there is a wrong move
    //Reprents the player's and entity's behaviour on collision
    void StumbleBehaviour(Entity entity)
    {
        //Change Player animator trigger State
        PlayerAnimatorRef.SetTrigger("stumble");

        //entity.GetComponent<Animator>().SetTrigger("stumble");
    }
    //Represens the behaviour that happens during the Acive Gameplay
    IEnumerator GameplayBehaviour()
    {
        //Set player Animation RunningSpeed Multiplier based on the GroundGenerator's MoveSpeed

        while (!GroundGeneratorRef.gameOver)
        {
            float calculatedMultiplier = GroundGeneratorRef.movingSpeed * (1 / GroundGeneratorRef.MinMovingSpeed);

            if (GroundGeneratorRef.movingSpeed >= 5)
            {
                //Reduce the CalculaedMultiplier value by some percentage 20%
                calculatedMultiplier -= (calculatedMultiplier * .2f);
            }

            PlayerAnimatorRef.SetFloat("speedMultiplier" , calculatedMultiplier); 
            yield return new WaitForEndOfFrame();
        }

        StopCoroutine(GameplayBehaviour());
    }
    #endregion
}
